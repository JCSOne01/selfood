﻿using System;
using System.Collections.Generic;
using SelFood.Services.Interfaces;
using Xamarin.Forms;

namespace SelFood
{
    public partial class RecoverPasswordPage : ContentPage
    {
        public RecoverPasswordPage()
        {
            InitializeComponent();
        }

        async void RecoverBtn_Clicked(System.Object sender, System.EventArgs e)
        {
            var authService = DependencyService.Get<IAuthService>();
            var response = await authService.RecoverPassword(EmailTxt.Text);
            if (response)
            {
                await DisplayAlert("Confirmación", "Se ha enviado un correo con instrucciones para cambiar la contraseña", "OK");
                return;
            }

            await DisplayAlert("Error", "Ha ocurrido un error o el usuario no existe", "OK");
        }
    }
}
